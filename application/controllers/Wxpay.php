<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wxpay extends CI_Controller{
	public function unifiedorder(){
		//准备微信支付提交订单接口参数
		$aPostData = array();
		$aPostData['appid']  = 'wx426b3015555a46be';
		$aPostData['mch_id'] = '1900009851';
		$aPostData['nonce_str'] = uniqid();
		$aPostData['body'] = "购买商品";
		$aPostData['product_id'] = time();
		$aPostData['out_trade_no'] = time();
		$aPostData['total_fee'] = 1;// 单位是分
		$aPostData['spbill_create_ip'] = '144.52.45.189';
		$aPostData['notify_url'] = 'http://106.14.188.41/notify_url.php';
		$aPostData['trade_type'] = 'NATIVE';

		$this->load->helper('common');
		$sSign = makeSign($aPostData);

		$aPostData['sign'] = $sSign;

		//生成xml格式数据
		$this->load->helper('xml');
		$sXml = toXml($aPostData);

		//xml传给微信支付接口
		$this->config->load('wxpay');
		$sUrl = $this->config->item('unifyorder_url');
		$this->load->helper('curl');
		$aRes = curl($sUrl, 'post', $sXml);
		
		//拿到支付结果了 先转换为数组格式
		$aRes = fromXml($aRes);
		
		$aViewData = array();
		$aViewData['code_url'] = $aRes['code_url'];
		$this->load->view('order_pay', $aViewData);
	}
}