<?php
function curl($sUrl, $sMethod = "get", $aData = array()){
	$ch = curl_init();

	if(strtolower($sMethod) == "get"){
		$sUrl .= "?" . http_build_query($aData);
	}

	curl_setopt($ch, CURLOPT_URL, $sUrl);

	if(strtolower($sMethod) == "post"){
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $aData);
	}

	//user-agent
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36');

	//连接时长 和 发送时长
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	//不验证https的证书
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER , 0);
	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$res = curl_exec($ch);
	if(!$res){
		$res = curl_errno($ch);
	}

	curl_close($ch);

	return $res;
}