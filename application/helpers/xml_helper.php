<?php
function toXml($values){
	if(!is_array($values) 
		|| count($values) <= 0){
		return false;
	}
	
	$xml = "<xml>";
	foreach ($values as $key=>$val)
	{
		if (is_numeric($val)){
			$xml.="<".$key.">".$val."</".$key.">";
		}else{
			$xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
		}
    }
    $xml.="</xml>";
    return $xml; 
}

function fromXml($xml){
    if(!$xml){
        return false;
    }
    //将XML转为array
    //禁止引用外部xml实体
    libxml_disable_entity_loader(true);
    return json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
}