<?php
include APPPATH . 'third_party/simple_html_dom.php';
class Movie extends CI_Model {
	private $_html_dom = null;
	private $_sPattern = "#https:\/\/movie.douban.com\/subject\/(\d+)#";

	private $_mapping = array(
		'movie_name' => 'span[property=v:itemreviewed]',
		'movie_rate' => 'strong[property=v:average]',
		'movie_director' => 'a[rel=v:directedBy]',
		'movie_main' => 'a[rel=v:starring]',
		'movie_type' => 'span[property=v:genre]',
		'movie_time' => 'span[property=v:initialReleaseDate]',
		'movie_length' => 'span[property=v:runtime]',
	);

	public function __construct(){
		$this->load->library("redisclient");
		$this->load->database();
		$this->load->helper('curl');
		$this->_html_dom = new simple_html_dom();
	}

	public function getMovieInfo($sUrl){
		if(empty($sUrl) || empty($this->_html_dom)){
			return false;
		}

		preg_match($this->_sPattern, $sUrl, $aMatch);
		if(empty($aMatch[1])){
			return false;
		}

		$res = curl($sUrl);
		$this->_html_dom->load($res);

		$aMovieInfo = array();
		$aMovieInfo['movie_id'] = $aMatch[1];

		//下面进行movie_id判重处理
		if($this->redisclient->isInSet('id_movie', $aMovieInfo['movie_id'])){
			return false;
		}

		$this->redisclient->addToSet('id_movie', $aMovieInfo['movie_id']);

		foreach ($this->_mapping as $sKey => $sValue) {
			$aMovieInfo[$sKey] =  $this->_html_dom->find($sValue, 0)->plaintext;
		}

		return $aMovieInfo;
	}

	public function getAllLinks($sUrl){
		if(empty($sUrl) || empty($this->_html_dom)){
			return false;
		}

		$res = curl($sUrl);
		$this->_html_dom->load($res);

		$aObj = $this->_html_dom->find('a[href]');

		$aUrls = array();
		foreach ($aObj as $obj) {
			$aUrls[] = $obj->href;
		}

		return $aUrls;
	}

	public function addMovie($aMovieInfo){
		if(empty($aMovieInfo['movie_id'])){
			return false;
		}

		$this->db->insert("movie_info", $aMovieInfo);

		return $this->db->insert_id();
	}
}