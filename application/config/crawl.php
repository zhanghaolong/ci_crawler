<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['url_start'] = 'https://movie.douban.com/';

$config['link_queue_name'] = 'link_queue';
$config['movieurl_queue_name'] = 'movie_queue';